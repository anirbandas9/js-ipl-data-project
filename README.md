# IPL Data Project I

**Download the data from:** [https://www.kaggle.com/manasgarg/ipl](https://www.kaggle.com/manasgarg/ipl)

There should be 2 files:
- deliveries.csv
- matches.csv

In this data assignment you will transform raw data of IPL to calculate the following stats:
1. Number of matches played per year for all the years in IPL.
2. Number of matches won per team per year in IPL.
3. Extra runs conceded per team in the year 2016
4. Top 10 economical bowlers in the year 2015
5. Find the number of times each team won the toss and also won the match
6. Find a player who has won the highest number of *Player of the Match* awards for each season
7. Find the strike rate of a batsman for each season
8. Find the highest number of times one player has been dismissed by another player
9. Find the bowler with the best economy in super overs

Implement the functions, **one for each task**.
Use the results of the functions to dump JSON files in the output folder

### Instructions:
- Create a new repo with name js-ipl-data-project in Gitlab subgroup, before starting implementation of the solution
- Make sure to follow proper Git practices
- Before submission, make sure that all the points in the below checklist are covered:
  - Git commits
  - Directory structure
  - package.json - dependencies, devDependencies
  - .gitignore file
  - Proper/Intuitive Variable names
  - Separate module for functions

### Directory structure:

- src/
  - server/
    - 1-matches-per-year.js
    - 2-matches-won-per-team-per-year.js
  - public/
    - output
        - matchesPerYear.json
        - ...
  - data/
    - matches.csv
    - deliveries.csv
- package.json
- package-lock.json
- .gitignore

<br>

## Running the code in local system

First need to clone the repository using the SSH/HTTPS by:

```sh
git clone git@gitlab.com:mountblue/23-1-python/anirban/js-ipl-data-project.git
```
Need to have npm installed for working of the project.

Installing all the packages required for this data project to run:

```sh
npm install
```

Running the code to get all the output as JSON files in the src/public/output folder:
```sh
node index.cjs
```
This will create all the JSON files as output.

<br>

## Output of all JSON files

### Problem 1

- Number of matches played per year for all the years in IPL.

```json
{
  "2008": 58,
  "2009": 57,
  "2010": 60,
  "2011": 73,
  "2012": 74,
  "2013": 76,
  "2014": 60,
  "2015": 59,
  "2016": 60,
  "2017": 59
}
```

### Problem 2

- Number of matches won per team per year in IPL.

```json
{
  "Sunrisers Hyderabad": {
    "2013": 10,
    "2014": 6,
    "2015": 7,
    "2016": 11,
    "2017": 8
  },
  "Rising Pune Supergiant": {
    "2017": 10
  },
    // continued...
}
```

### Problem 3

- Extra runs conceded per team in the year 2016.

```json
{
  "Rising Pune Supergiants": 108,
  "Mumbai Indians": 102,
  "Kolkata Knight Riders": 122,
  "Delhi Daredevils": 106,
  "Gujarat Lions": 98,
  "Kings XI Punjab": 100,
  "Sunrisers Hyderabad": 107,
  "Royal Challengers Bangalore": 156
}
```

### Problem 4

- Top 10 economical bowlers in the year 2015

```json
{
  "RN ten Doeschate": {
    "runs_conceded": 4,
    "total_balls": 6,
    "economy": 4
  },
  "J Yadav": {
    "runs_conceded": 29,
    "total_balls": 42,
    "economy": 4.14
  },
  "V Kohli": {
    "runs_conceded": 10,
    "total_balls": 11,
    "economy": 5.45
  },
  "R Ashwin": {
    "runs_conceded": 228,
    "total_balls": 234,
    "economy": 5.85
  },
  "S Nadeem": {
    "runs_conceded": 43,
    "total_balls": 42,
    "economy": 6.14
  },
  "Parvez Rasool": {
    "runs_conceded": 31,
    "total_balls": 30,
    "economy": 6.2
  },
  "MC Henriques": {
    "runs_conceded": 158,
    "total_balls": 150,
    "economy": 6.32
  },
  "Z Khan": {
    "runs_conceded": 156,
    "total_balls": 145,
    "economy": 6.46
  },
  "MA Starc": {
    "runs_conceded": 291,
    "total_balls": 258,
    "economy": 6.77
  },
  "GB Hogg": {
    "runs_conceded": 144,
    "total_balls": 126,
    "economy": 6.86
  }
}
```

### Problem 5

- Find the number of times each team won the toss and also won the match.

```json
{
  "Rising Pune Supergiant": 5,
  "Kolkata Knight Riders": 44,
  "Kings XI Punjab": 28,
  "Royal Challengers Bangalore": 35,
  "Sunrisers Hyderabad": 17,
  "Mumbai Indians": 48,
  "Gujarat Lions": 10,
  "Delhi Daredevils": 33,
  "Chennai Super Kings": 42,
  "Rajasthan Royals": 34,
  "Deccan Chargers": 19,
  "Kochi Tuskers Kerala": 4,
  "Pune Warriors": 3,
  "Rising Pune Supergiants": 3
}
```

### Problem 6

- Find a player who has won the highest number of Player of the Match awards for each season.

```json
{
  "2008": [
    "SE Marsh",
    5
  ],
  "2009": [
    "YK Pathan",
    3
  ],
  "2010": [
    "SR Tendulkar",
    4
  ],
  "2011": [
    "CH Gayle",
    6
  ],
  "2012": [
    "CH Gayle",
    5
  ],
  "2013": [
    "MEK Hussey",
    5
  ],
  "2014": [
    "GJ Maxwell",
    4
  ],
  "2015": [
    "DA Warner",
    4
  ],
  "2016": [
    "V Kohli",
    5
  ],
  "2017": [
    "BA Stokes",
    3
  ]
}
```

### Problem 7

- Find the strike rate of a batsman for each season.

```json
{
  "DA Warner": {
    "2009": {
      "runs_scored": 163,
      "balls_faced": 137,
      "strike_rate": 118.98
    },
    "2010": {
      "runs_scored": 282,
      "balls_faced": 195,
      "strike_rate": 144.62
    },
    "2011": {
      "runs_scored": 324,
      "balls_faced": 281,
      "strike_rate": 115.3
    },
    "2012": {
      "runs_scored": 256,
      "balls_faced": 161,
      "strike_rate": 159.01
    },
    "2013": {
      "runs_scored": 410,
      "balls_faced": 333,
      "strike_rate": 123.12
    },
    "2014": {
      "runs_scored": 528,
      "balls_faced": 386,
      "strike_rate": 136.79
    },
    "2015": {
      "runs_scored": 562,
      "balls_faced": 368,
      "strike_rate": 152.72
    },
    "2016": {
      "runs_scored": 848,
      "balls_faced": 579,
      "strike_rate": 146.46
    },
    "2017": {
      "runs_scored": 641,
      "balls_faced": 461,
      "strike_rate": 139.05
    }
  }
    // continued
}
```

### Problem 8

- Find the highest number of times one player has been dismissed by another player.

```json
[
  "MS Dhoni - dismissed by - Z Khan",
  7
]
```

### Problem 9

- Find the bowler with the best economy in super overs.

```json
[
  "JJ Bumrah",
  {
    "runs_conceded": 4,
    "total_balls": 8,
    "economy": 3
  }
]
```