// Importing file system module for writing output into JSON files.
const fs = require("fs");
// Importing csvtojson for reading csv data into JSON file.
const csv = require("csvtojson");

// File paths of the csv data
const csvFilePath1 = "./src/data/matches.csv";
const csvFilePath2 = "./src/data/deliveries.csv";

// Importing problems function into each variable.
const matchesPlayedPerYear = require("./src/server/matchesPlayedPerYear.cjs");
const matchesWonPerTeamPerYear = require("./src/server/matchesWonPerTeamPerYear.cjs");
const extraRunsConcededPerTeam2016 = require("./src/server/extraRunsConcededPerTeam2016.cjs");
const top10EconomicBowlers2015 = require("./src/server/top10EconomicBowlers2015.cjs");
const teamWonTossAndMatch = require("./src/server/teamWonTossAndMatch.cjs");
const playerOfTheMatchAwardsPerSeason = require("./src/server/playerOfTheMatchAwardsPerSeason.cjs");
const strikeRateOfBatsmanPerSeason = require("./src/server/strikeRateOfBatsmanPerSeason.cjs");
const playerDismissedByBowler = require('./src/server/playerDismissedByBowler.cjs');
const bestEconomyInSuperOvers = require('./src/server/bestEconomyInSuperOvers.cjs');

// Function to write result into JSON file using JSON stringify, it takes two parameters (path of the file and the output) 
function writeJson(filePath, fileName) {
  fs.writeFileSync(filePath, JSON.stringify(fileName, null, 2), (err) => {
    if (err) console.log(err);
  });
}

// Read csv data into JSON format using csv-to-json library and calling each problem function and writing it into JSON file.
csv()
  .fromFile(csvFilePath1)
  // reading matches csv data and storing it in matches
  .then((matches) => {
    csv()
      .fromFile(csvFilePath2)
      // reading deliveries csv data and storing it in deliveries
      .then((deliveries) => {
        // Problem 1

        const output1 = matchesPlayedPerYear(matches);
        const filePath1 = "./src/public/output/matchesPlayedPerYear.json";
        writeJson(filePath1, output1);

        // Problem 2

        const output2 = matchesWonPerTeamPerYear(matches);
        const filePath2 = "./src/public/output/matchesWonPerTeamPerYear.json";
        writeJson(filePath2, output2);

        // Problem 3

        const output3 = extraRunsConcededPerTeam2016(matches, deliveries);
        const filePath3 =
          "./src/public/output/extraRunsConcededPerTeam2016.json";
        writeJson(filePath3, output3);

        // Problem 4

        const output4 = top10EconomicBowlers2015(matches, deliveries);
        const filePath4 = "./src/public/output/top10EconomicBowlers2015.json";
        writeJson(filePath4, output4);

        // Problem 5

        const output5 = teamWonTossAndMatch(matches);
        const filePath5 = "./src/public/output/teamWonTossAndMatch.json";
        writeJson(filePath5, output5);

        // Problem 6

        const output6 = playerOfTheMatchAwardsPerSeason(matches);
        const filePath6 = "./src/public/output/playerOfTheMatchAwardsPerSeason.json";
        writeJson(filePath6, output6);

        // Problem 7

        const output7 = strikeRateOfBatsmanPerSeason(matches, deliveries);
        const filePath7 = "./src/public/output/strikeRateOfBatsmanPerSeason.json";
        writeJson(filePath7, output7);

        // Problem 8

        const output8 = playerDismissedByBowler(deliveries);
        const filePath8 = "./src/public/output/playerDismissedByBowler.json";
        writeJson(filePath8, output8);

        // Problem 9

        const output9 = bestEconomyInSuperOvers(deliveries);
        const filePath9 = "./src/public/output/bestEconomyInSuperOvers.json";
        writeJson(filePath9, output9);
      });
  });