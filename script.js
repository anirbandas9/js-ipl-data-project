// Problem - 1
fetch("./src/public/output/matchesPlayedPerYear.json")
  .then((response) => response.json())
  .then((matchesPlayed) => {
    const season = Object.keys(matchesPlayed);
    const matchesArr = Object.values(matchesPlayed);

    Highcharts.chart("container", {
      chart: {
        type: "column",
      },
      title: {
        text: "Total Matches Played Per Year",
      },
      xAxis: {
        title: {
          text: "Years",
        },
        categories: season,
      },
      yAxis: {
        title: {
          text: "Matches Played",
        },
      },
      tooltip: {
        backgroundColor: "#FCFFC5",
        borderColor: "black",
        borderRadius: 10,
        borderWidth: 3,
      },
      series: [
        {
          name: "Matches",
          data: matchesArr,
          color: "grey",
          // visible: false
        },
      ],
    });
  });

// Problem - 2
fetch("./src/public/output/matchesWonPerTeamPerYear.json")
  .then((response) => response.json())
  .then((matchesWonPerTeam) => {
    // Find all unique teams in the data
    const allTeams = Object.values(matchesWonPerTeam).flatMap(Object.keys);
    const uniqueTeams = [...new Set(allTeams)];
    // Define the series data in the format expected by Highcharts
    const seriesData = uniqueTeams.map((team) => {
      return {
        name: team,
        data: Object.keys(matchesWonPerTeam).map(
          (year) => matchesWonPerTeam[year][team] || 0
        ),
      };
    });
    // Define the categories (year) for the X axis
    const categories = Object.keys(matchesWonPerTeam);
    // Create the chart
    Highcharts.chart("container1", {
      chart: {
        type: "column",
      },
      title: {
        text: "Number Of Matches Won Per Team Per Year",
      },
      xAxis: {
        categories: categories,
        crosshair: true,
      },
      tooltip: {
        backgroundColor: "#FCFFC5",
        borderColor: "black",
        borderRadius: 10,
        borderWidth: 3,
        shared: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Wins",
        },
      },
      series: seriesData,
      color: "grey",
    });
  });

// Problem - 3
fetch("./src/public/output/extraRunsConcededPerTeam2016.json")
  .then((response) => response.json())
  .then((extraRunsConceded) => {
    const teams = Object.keys(extraRunsConceded);
    const runs = Object.values(extraRunsConceded);
    Highcharts.chart("container2", {
      chart: {
        type: "column",
      },
      title: {
        text: "Extra Runs Conceded By Team In The Year 2016",
      },
      xAxis: {
        title: {
          text: "Teams",
        },
        categories: teams,
      },
      yAxis: {
        title: {
          text: "Extra Runs",
        },
      },
      tooltip: {
        backgroundColor: "#FCFFC5",
        borderColor: "black",
        borderRadius: 10,
        borderWidth: 3,
      },
      series: [
        {
          name: "Extra Runs Conceded",
          data: runs,
          color: "grey",
          // visible: false
        },
      ],
    });
  });

// Problem - 4
fetch("./src/public/output/top10EconomicBowlers2015.json")
  .then((response) => response.json())
  .then((top10EconomicBowler) => {
    const player = Object.keys(top10EconomicBowler);
    const economy = Object.entries(top10EconomicBowler).reduce(
      (acc, player) => {
        acc.push(player[1].economy);
        return acc;
      },
      []
    );

    Highcharts.chart("container3", {
      chart: {
        type: "column",
      },
      title: {
        text: "Top 10 Economic Bowlers In The Year 2016",
      },
      xAxis: {
        title: {
          text: "Bowlers",
        },
        categories: player,
      },
      yAxis: {
        title: {
          text: "Economic Rate",
        },
      },
      tooltip: {
        backgroundColor: "#FCFFC5",
        borderColor: "black",
        borderRadius: 10,
        borderWidth: 3,
      },
      series: [
        {
          name: "Economic Rate",
          data: economy,
          color: "grey",
          // visible: false
        },
      ],
    });
  });

// Problem - 5
fetch("./src/public/output/teamWonTossAndMatch.json")
  .then((response) => response.json())
  .then((teamWonTossAndMatch) => {
    const teams = Object.keys(teamWonTossAndMatch);
    const matchesWon = Object.values(teamWonTossAndMatch);

    Highcharts.chart("container4", {
      chart: {
        type: "column",
      },
      title: {
        text: "Team Won Toss And Match Together",
      },
      xAxis: {
        title: {
          text: "Teams",
        },
        categories: teams,
      },
      yAxis: {
        title: {
          text: "Total Won Matches and Toss",
        },
      },
      tooltip: {
        backgroundColor: "#FCFFC5",
        borderColor: "black",
        borderRadius: 10,
        borderWidth: 3,
      },
      series: [
        {
          name: "Matches Won",
          data: matchesWon,
          color: "grey",
          // visible: false
        },
      ],
    });
  });

// Problem - 6
fetch("./src/public/output/playerOfTheMatchAwardsPerSeason.json")
  .then((response) => response.json())
  .then((playerOfTheMatchAwards) => {
    const years = Object.keys(playerOfTheMatchAwards);
    // Create the chart
    Highcharts.chart("container5", {
      chart: {
        type: "column",
      },
      title: {
        text: "Player Of The Match Awards Per Season",
      },
      xAxis: {
        categories: years,
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Awards Won",
        },
      },
      tooltip: {
        backgroundColor: "#FCFFC5",
        borderColor: "black",
        borderRadius: 10,
        borderWidth: 3,
      },
      series: [
        {
          name: "Awards",
          data: years.map((year) => playerOfTheMatchAwards[year]),
          color: "grey",
        },
      ],
    });
  });

// Problem - 7
fetch("./src/public/output/strikeRateOfBatsmanPerSeason.json")
  .then((response) => response.json())
  .then((strikeRateOfBatsman) => {
    // Find all unique players in the data
    const allPlayers = Object.values(strikeRateOfBatsman).flatMap(Object.keys);
    const uniquePlayers = [...new Set(allPlayers)];

    const seriesData = uniquePlayers.map((player) => {
      return {
        name: player,
        data: Object.keys(strikeRateOfBatsman).map(
          (year) => strikeRateOfBatsman[year][player] || 0
        ),
      };
    });

    const categories = Object.keys(strikeRateOfBatsman);

    // Create the chart
    Highcharts.chart("container6", {
      chart: {
        type: "column",
      },
      title: {
        text: "Strike Rate of Batsman For Each Season",
      },
      tooltip: {
        backgroundColor: "#FCFFC5",
        borderColor: "black",
        borderRadius: 10,
        borderWidth: 3,
      },
      xAxis: {
        // title: {
        //   text: "Season",
        // },
        categories: categories,
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Strike Rate",
        },
      },
      series: seriesData,
    });
  });

// Problem - 8
fetch("./src/public/output/playerDismissedByBowler.json")
  .then((response) => response.json())
  .then((playerDismissedByBowler) => {
    const player = Object.keys(playerDismissedByBowler);
    const economy = Object.entries(playerDismissedByBowler).reduce(
      (acc, player) => {
        acc.push(player[1].economy);
        return acc;
      },
      []
    );

    Highcharts.chart("container7", {
      chart: {
        type: "column",
      },
      title: {
        text: "Top 10 Player Dismissed By Bowler",
      },
      xAxis: {
        title: {
          text: "Bowlers",
        },
        categories: playerDismissedByBowler
          .map((player) => player[0])
          .slice(0, 10),
      },
      yAxis: {
        title: {
          text: "Dismissal Count",
        },
      },
      tooltip: {
        backgroundColor: "#FCFFC5",
        borderColor: "black",
        borderRadius: 10,
        borderWidth: 3,
      },
      series: [
        {
          name: "Dismissal",
          data: playerDismissedByBowler.map((player) => player[1]).slice(0, 10),
          color: "grey",
          // visible: false
        },
      ],
    });
  });

// Problem - 9
fetch("./src/public/output/bestEconomyInSuperOvers.json")
  .then((response) => response.json())
  .then((economyInSuperOvers) => {
    const player = Object.keys(economyInSuperOvers);
    const economy = Object.entries(economyInSuperOvers).reduce(
      (acc, player) => {
        acc.push(player[1].economy);
        return acc;
      },
      []
    );

    Highcharts.chart("container8", {
      chart: {
        type: "column",
      },
      title: {
        text: "Best Economy In Super Over",
      },
      tooltip: {
        backgroundColor: "#FCFFC5",
        borderColor: "black",
        borderRadius: 10,
        borderWidth: 3,
      },
      xAxis: {
        title: {
          text: "Bowlers",
        },
        categories: player,
        crosshair: true,
      },
      yAxis: {
        title: {
          text: "Economic Rate",
        },
      },
      series: [
        {
          name: "Economic Rate",
          data: economy,
          color: "grey",
          // visible: false
        },
      ],
    });
  });

// import matchesPlayed from "./src/public/output/matchesPlayedPerYear.json" assert { type: "json" };
// import matchesWonPerTeam from "./src/public/output/matchesWonPerTeamPerYear.json" assert { type: "json" };
// import extraRunsConceded from './src/public/output/extraRunsConcededPerTeam2016.json' assert { type: "json"};
// import top10EconomicBowler from './src/public/output/top10EconomicBowlers2015.json' assert { type: "json"};
// import teamWonTossAndMatch from './src/public/output/teamWonTossAndMatch.json' assert { type: "json"};
// import playerOfTheMatchAwards from './src/public/output/playerOfTheMatchAwardsPerSeason.json' assert { type: "json"};
// import strikeRateOfBatsman from './src/public/output/strikeRateOfBatsmanPerSeason.json' assert { type: "json"};
// import playerDismissedByBowler from './src/public/output/playerDismissedByBowler.json' assert { type: "json"};
// import economyInSuperOvers from './src/public/output/bestEconomyInSuperOvers.json' assert { type: "json"};

// // Problem - 1
// document.addEventListener("DOMContentLoaded", function () {
//   const season = Object.keys(matchesPlayed);
//   const matchesArr = Object.values(matchesPlayed);

//   Highcharts.chart("container", {
//     chart: {
//       type: "column",
//     },
//     title: {
//       text: "Total Matches Played Per Year",
//     },
//     xAxis: {
//       title: {
//         text: "Years",
//       },
//       categories: season,
//     },
//     yAxis: {
//       title: {
//         text: "Matches Played",
//       },
//     },
//     tooltip: {
//       backgroundColor: "#FCFFC5",
//       borderColor: "black",
//       borderRadius: 10,
//       borderWidth: 3,
//     },
//     series: [
//       {
//         name: "Matches",
//         data: matchesArr,
//         color: "grey",
//         // visible: false
//       },
//     ],
//   });
// });

// // Problem - 2
// document.addEventListener("DOMContentLoaded", function () {
//   // Find all unique teams in the data
//   const allTeams = Object.values(matchesWonPerTeam).flatMap(Object.keys);
//   const uniqueTeams = [...new Set(allTeams)];

//   // Define the series data in the format expected by Highcharts
//   const seriesData = uniqueTeams.map((team) => {
//     return {
//       name: team,
//       data: Object.keys(matchesWonPerTeam).map(
//         (year) => matchesWonPerTeam[year][team] || 0
//       ),
//     };
//   });

//   // Define the categories (year) for the X axis
//   const categories = Object.keys(matchesWonPerTeam);
//   // Create the chart
//   Highcharts.chart("container1", {
//     chart: {
//       type: "column",
//     },
//     title: {
//       text: "Number Of Matches Won Per Team Per Year",
//     },
//     xAxis: {
//       categories: categories,
//       crosshair: true,
//     },
//     tooltip: {
//       backgroundColor: "#FCFFC5",
//       borderColor: "black",
//       borderRadius: 10,
//       borderWidth: 3,
//       shared: true,
//     },
//     yAxis: {
//       min: 0,
//       title: {
//         text: "Wins",
//       },
//     },
//     series: seriesData,
//     color: "grey",
//   });
// });

// // Problem - 3
// document.addEventListener("DOMContentLoaded", function () {
//   const teams = Object.keys(extraRunsConceded);
//   const runs = Object.values(extraRunsConceded);
//   Highcharts.chart("container2", {
//     chart: {
//       type: "column",
//     },
//     title: {
//       text: "Extra Runs Conceded By Team In The Year 2016",
//     },
//     xAxis: {
//       title: {
//         text: "Teams",
//       },
//       categories: teams,
//     },
//     yAxis: {
//       title: {
//         text: "Extra Runs",
//       },
//     },
//     tooltip: {
//       backgroundColor: "#FCFFC5",
//       borderColor: "black",
//       borderRadius: 10,
//       borderWidth: 3,
//     },
//     series: [
//       {
//         name: "Extra Runs Conceded",
//         data: runs,
//         color: "grey",
//         // visible: false
//       },
//     ],
//   });
// });

// // Problem - 4
// document.addEventListener("DOMContentLoaded", function () {
//   const player = Object.keys(top10EconomicBowler);
//   const economy = Object.entries(top10EconomicBowler).reduce((acc, player) => {
//     acc.push(player[1].economy);
//     return acc;
//   }, []);

//   Highcharts.chart("container3", {
//     chart: {
//       type: "column",
//     },
//     title: {
//       text: "Top 10 Economic Bowlers In The Year 2016",
//     },
//     xAxis: {
//       title: {
//         text: "Bowlers",
//       },
//       categories: player,
//     },
//     yAxis: {
//       title: {
//         text: "Economic Rate",
//       },
//     },
//     tooltip: {
//       backgroundColor: "#FCFFC5",
//       borderColor: "black",
//       borderRadius: 10,
//       borderWidth: 3,
//     },
//     series: [
//       {
//         name: "Economic Rate",
//         data: economy,
//         color: "grey",
//         // visible: false
//       },
//     ],
//   });
// });

// // Problem - 5
// document.addEventListener("DOMContentLoaded", function () {
//   const teams = Object.keys(teamWonTossAndMatch);
//   const matchesWon = Object.values(teamWonTossAndMatch);

//   Highcharts.chart("container4", {
//     chart: {
//       type: "column",
//     },
//     title: {
//       text: "Team Won Toss And Match Together",
//     },
//     xAxis: {
//       title: {
//         text: "Teams",
//       },
//       categories: teams,
//     },
//     yAxis: {
//       title: {
//         text: "Total Won Matches and Toss",
//       },
//     },
//     tooltip: {
//       backgroundColor: "#FCFFC5",
//       borderColor: "black",
//       borderRadius: 10,
//       borderWidth: 3,
//     },
//     series: [
//       {
//         name: "Matches Won",
//         data: matchesWon,
//         color: "grey",
//         // visible: false
//       },
//     ],
//   });
// });

// // Problem - 6
// document.addEventListener("DOMContentLoaded", function () {
//   const years = Object.keys(playerOfTheMatchAwards);
//   // Create the chart
//   Highcharts.chart("container5", {
//     chart: {
//       type: "column",
//     },
//     title: {
//       text: "Player Of The Match Awards Per Season",
//     },
//     xAxis: {
//       categories: years,
//       crosshair: true,
//     },
//     yAxis: {
//       min: 0,
//       title: {
//         text: "Awards Won",
//       },
//     },
//     tooltip: {
//       backgroundColor: "#FCFFC5",
//       borderColor: "black",
//       borderRadius: 10,
//       borderWidth: 3,
//     },
//     series: [
//       {
//         name: "Awards",
//         data: years.map((year) => playerOfTheMatchAwards[year]),
//         color: "grey",
//       },
//     ],
//   });
// });

// // Problem - 7
// document.addEventListener("DOMContentLoaded", function () {
//   // Find all unique players in the data
//   const allPlayers = Object.values(strikeRateOfBatsman).flatMap(Object.keys);
//   const uniquePlayers = [...new Set(allPlayers)];

//   const seriesData = uniquePlayers.map((player) => {
//     return {
//       name: player,
//       data: Object.keys(strikeRateOfBatsman).map(
//         (year) => strikeRateOfBatsman[year][player] || 0
//       ),
//     };
//   });

//   const categories = Object.keys(strikeRateOfBatsman);

//   // Create the chart
//   Highcharts.chart("container6", {
//     chart: {
//       type: "column",
//     },
//     title: {
//       text: "Strike Rate of Batsman For Each Season",
//     },
//     tooltip: {
//       backgroundColor: "#FCFFC5",
//       borderColor: "black",
//       borderRadius: 10,
//       borderWidth: 3,
//     },
//     xAxis: {
//       // title: {
//       //   text: "Season",
//       // },
//       categories: categories,
//       crosshair: true,
//     },
//     yAxis: {
//       min: 0,
//       title: {
//         text: "Strike Rate",
//       },
//     },
//     series: seriesData,
//   });
// });

// // Problem - 8
// document.addEventListener("DOMContentLoaded", function () {
//   const player = Object.keys(economyInSuperOvers);
//   const economy = Object.entries(economyInSuperOvers).reduce((acc, player) => {
//     acc.push(player[1].economy);
//     return acc;
//   }, []);

//   Highcharts.chart("container7", {
//     chart: {
//       type: "column",
//     },
//     title: {
//       text: "Top 10 Player Dismissed By Bowler",
//     },
//     xAxis: {
//       title: {
//         text: "Bowlers",
//       },
//       categories: playerDismissedByBowler
//         .map((player) => player[0])
//         .slice(0, 10),
//     },
//     yAxis: {
//       title: {
//         text: "Dismissal Count",
//       },
//     },
//     tooltip: {
//       backgroundColor: "#FCFFC5",
//       borderColor: "black",
//       borderRadius: 10,
//       borderWidth: 3,
//     },
//     series: [
//       {
//         name: "Dismissal",
//         data: playerDismissedByBowler.map((player) => player[1]).slice(0, 10),
//         color: "grey",
//         // visible: false
//       },
//     ],
//   });
// });

// // Problem - 9
// document.addEventListener("DOMContentLoaded", function () {
//   const player = Object.keys(economyInSuperOvers);
//   const economy = Object.entries(economyInSuperOvers).reduce((acc, player) => {
//     acc.push(player[1].economy);
//     return acc;
//   }, []);

//   Highcharts.chart("container8", {
//     chart: {
//       type: "column",
//     },
//     title: {
//       text: "Best Economy In Super Over",
//     },
//     tooltip: {
//       backgroundColor: "#FCFFC5",
//       borderColor: "black",
//       borderRadius: 10,
//       borderWidth: 3,
//     },
//     xAxis: {
//       title: {
//         text: "Bowlers",
//       },
//       categories: player,
//       crosshair: true,
//     },
//     yAxis: {
//       title: {
//         text: "Economic Rate",
//       },
//     },
//     series: [
//       {
//         name: "Economic Rate",
//         data: economy,
//         color: "grey",
//         // visible: false
//       },
//     ],
//   });
// });
