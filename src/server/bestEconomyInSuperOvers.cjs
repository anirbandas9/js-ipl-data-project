// 9 - Find the bowler with the best economy in super overs

function bestEconomyInSuperOvers(deliveries) {
    // If deliveries data is empty or not an array it will return an empty object.
    if (deliveries === undefined || Array.isArray(deliveries) === false) {
        return {};
    }

    // Find the economy bowler stat in super over which contains runs conceded, balls and economy.
    const result = deliveries.reduce((accumulator, delivery) => {
        const isSuperOver = Number(delivery.is_super_over);
        const bowler = delivery.bowler;
        const totalRuns = Number(delivery.total_runs);
        const byeRuns = Number(delivery.bye_runs);
        const legByeRuns = Number(delivery.legbye_runs);

        if (isSuperOver === 1) {
            if (bowler in accumulator) {
                accumulator[bowler]['runs_conceded'] += (totalRuns - byeRuns - legByeRuns);
                accumulator[bowler]['total_balls'] += 1;
            }
            else {
                accumulator[bowler] = {
                    runs_conceded: (totalRuns - byeRuns - legByeRuns),
                    total_balls: 1,
                    economy: 0,
                };
            }
        }

        return accumulator;
    }, {});

    // To find the best economic bowler in super over and sort it.
    const bestEconomyBowler = Object.entries(result)
        .map((bowler) => {
            bowler[1]['economy'] = Number((bowler[1]['runs_conceded'] / bowler[1]['total_balls'] * 6).toFixed(2));
            return bowler;
        })
        .sort((playerA, playerB) => playerA[1]['economy'] - playerB[1]['economy'])
        .slice(0, 10);

    // return only the top economy bowler
    return Object.fromEntries(bestEconomyBowler);
}

module.exports = bestEconomyInSuperOvers;