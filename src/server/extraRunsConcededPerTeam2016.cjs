// 3 - Extra runs conceded per team in the year 2016

function extraRunsConcededPerTeam2016(matches, deliveries) {
    // If any data JSON is empty or not an array it will return an empty object.
    if (matches === undefined || !Array.isArray(matches) || deliveries === undefined || !Array.isArray(deliveries)) {
        return {};
    }

    // To find and store the unique match id of the season 2016 using set collection.
    const uniqueId = matches.reduce((accumulator, match) => {
        const matchId = match.id;
        const season = Number(match.season);

        if (season === 2016) {
            accumulator.add(matchId);
        }
        return accumulator;
    },new Set());

    // To find the extra runs conceded by the bowling team an store it in an object.
    const extraRunsConcededObj = deliveries.reduce((accumulator, delivery) => {
        const matchId = delivery.match_id;
        const bowlingTeam = delivery.bowling_team;
        const extraRuns = Number(delivery.extra_runs);

        if (uniqueId.has(matchId)) {
            accumulator[bowlingTeam] = accumulator[bowlingTeam] + extraRuns || extraRuns;
        }
        return accumulator;
    }, {});
    
    return extraRunsConcededObj;
}

module.exports = extraRunsConcededPerTeam2016;