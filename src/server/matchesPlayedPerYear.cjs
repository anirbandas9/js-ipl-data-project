// 1 - Number of matches played per year for all the years in IPL.

function matchesPlayedPerYear(matches) {
    // If matches data is empty or not an array it will return empty object;
    if (matches === undefined || Array.isArray(matches) === false) {
        return {};
    }

    // To find matches played per year in IPL and storing matchesPlayedObj as an object.
    const matchesPlayedObj = matches.reduce((accumulator, match) => {
        const season = match.season;
        accumulator[season] = accumulator[season] + 1 || 1;
        return accumulator;
    }, {}); 

    // Returning the matchesPlayedObj as an object.
    return matchesPlayedObj;
}

module.exports = matchesPlayedPerYear;