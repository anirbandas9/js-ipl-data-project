// 2 - Number of matches won per team per year in IPL.

function matchesWonPerTeamPerYear(matches) {
    // If matches data is empty or not an array it will return an empty object.
    if (matches === undefined || Array.isArray(matches) === false) {
        return {};
    }
    
    // It will stored the matches won by team in each year by storing team as an object and matches won as nested object.
    const matchesWonByTeamObj = matches.reduce((accumulator, match) => {
        const season = match.season;
        const matchWinner = match.winner;
        
        // check if match winner is not empty or null and it will store all team with their total matches won.
        if (matchWinner !== undefined && season !== undefined && matchWinner !== '' && season !== '') {
            if (season in accumulator) {
                accumulator[season][matchWinner] = accumulator[season][matchWinner] + 1 || 1;
            }
            else {
                accumulator[season] = {};
                accumulator[season][matchWinner] = 1;
            }
        }
        return accumulator;
    }, {});

    // Returning the team with their total matches won per season in IPL.
    return matchesWonByTeamObj;
}

module.exports = matchesWonPerTeamPerYear;