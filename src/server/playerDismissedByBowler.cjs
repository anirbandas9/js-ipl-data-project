// 8 - Find the highest number of times one player has been dismissed by another player

function playerDismissedByBowler(deliveries) {
    // If the deliveries data is empty or not an array it will return an empty object.
    if (deliveries === undefined || Array.isArray(deliveries) === false) {
        return {};
    }

    // It will return an object that contains the player dismissed by bowler as a key and total dismissal as a value.
    const playerDismissed = deliveries.reduce((accumulator, delivery) => {
        const playerOut = delivery.player_dismissed;
        const bowler = delivery.bowler;
        const dismissalKind = delivery.dismissal_kind;
        const specialDismissedWay = ['run out', 'retired hurt', 'obstructing the field'];

        // Concating the player dismissed with the bowler as a key and total times as a value.
        if (playerOut !== undefined && playerOut !== '' && specialDismissedWay.includes(dismissalKind) === false) {
            accumulator[bowler] = accumulator[bowler] || {};
            accumulator[bowler][playerOut] = accumulator[bowler][playerOut] + 1 || 1;
        }
        return accumulator;
    }, {});

    // Sorting the player dismissal object in decreasing order of highest dismissal at the top.
    const result = Object.entries(playerDismissed)
        .reduce((accumulator, bowler) => {
            accumulator = accumulator.concat(Object.entries(bowler[1]).map((batsman) => {
                return [bowler[0], batsman];
            }));
            return accumulator;
        }, []).sort((playerA, playerB) => playerB[1][1] - playerA[1][1]);

    // returning only the top dismissed player by the bowler name.
    return result;
}

module.exports = playerDismissedByBowler;