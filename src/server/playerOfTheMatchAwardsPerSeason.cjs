// 6 - Find a player who has won the highest number of Player of the Match awards for each season

function playerOfTheMatchAwardsPerSeason(matches) {
    // If the matches data is empty or not an array it will return an empty object.
    if (matches === undefined || Array.isArray(matches) === false) {
        return {};
    }

    // This will store an object of season with the total match awards of each players in that season.
    const playerOfMatchSeasonObj = matches.reduce((accumulator, match) => {
        const season = match.season;
        const playerOfTheMatch = match.player_of_match;

        // It will store season in the object with the total player match awards as a nested object of each season.
        if (season in accumulator) {
            accumulator[season][playerOfTheMatch] = accumulator[season][playerOfTheMatch] + 1 || 1;
        }
        else {
            accumulator[season] = {};
            accumulator[season][playerOfTheMatch] = 1;
        }
        return accumulator;
    }, {});

    // It will store the player with the match awards in decreasing order and return only the top players per season.
    const sortedPlayerOfMatchSeason = Object.entries(playerOfMatchSeasonObj)
        .map((season) => {
            const playerList = (Object.entries(season[1])
                .sort((playerA, playerB) => playerB[1] - playerA[1]));
            return [season[0], playerList[0]];
        });
    
    // Converting the top player match awards per season from array to object and returning it.
    return Object.fromEntries(sortedPlayerOfMatchSeason);
}

module.exports = playerOfTheMatchAwardsPerSeason;