// 7 - Find the strike rate of a batsman for each season

function strikeRateOfBatsmanPerSeason(matches, deliveries) {
    // If any data JSON is empty or not an array it will return an empty object.
    if (matches === undefined || deliveries === undefined || !Array.isArray(matches) || !Array.isArray(deliveries)) {
        return {};
    }

    // This is store an object of all the matches id with their respective season played as a value.
    const matchIdSeason = matches.reduce((accumulator, match) => {
        const matchId = match.id;
        const season = match.season;

        if (season !== undefined && matchId !== undefined) {
            accumulator[matchId] = season;
        }
        return accumulator;
    }, {});

    // To store batsman stats as an object of player with each year runs scored, ball faced and strike rate as a nested object.
    const batsmanStats = deliveries.reduce((accumulator, delivery) => {
        const batsman = delivery.batsman;
        const matchId = delivery.match_id;
        const batsmanRuns = Number(delivery.batsman_runs);
        const wideRuns = Number(delivery.wide_runs);
        const season = matchIdSeason[matchId];

        // If batsman found than inside the result object it will add the batsman runs and the balls faced as an object.
        if (season in accumulator) {
            if (batsman in accumulator[season]) {
                accumulator[season][batsman]['runs_scored'] += batsmanRuns;
                if (wideRuns === 0) {
                    accumulator[season][batsman]['balls_faced'] += 1; 
                }
            }
            else {
                accumulator[season][batsman] = {
                    runs_scored: batsmanRuns,
                    balls_faced: 1,
                    strike_rate: 0
                };
            }
        }
        else {
            accumulator[season] = {};
        }
        return accumulator;
    }, {});

    // This will convert the object into an array to find the batsman strike rate and return it.
    const batsmanStrikeRate = Object.entries(batsmanStats)
        .map((season) => {
            const seasonList = Object.entries(season[1])
                .reduce((acc ,player) => {
                    acc[player[0]] = Number((player[1]['runs_scored'] / player[1]['balls_faced'] * 100).toFixed(2));
                    return acc;
                }, {});
            return [season[0], seasonList];
        });

    // Convert the batsman strike rate from array to an object and return it.
    return Object.fromEntries(batsmanStrikeRate);
}

module.exports = strikeRateOfBatsmanPerSeason;