// 5 - Find the number of times each team won the toss and also won the match

function teamWonTossAndMatch(matches) {
    // If matches data is empty or not an array it will return empty object;
    if (matches === undefined || Array.isArray(matches) === false) {
        return {};
    }

    // This will find the team who won both the match and the toss and store it in an object.
    const result = matches.reduce((accumulator, match) => {
        const tossWinner = match.toss_winner;
        const matchWinner = match.winner;

        if (tossWinner !== undefined && matchWinner !== undefined) {
            if (tossWinner === matchWinner) {
                accumulator[tossWinner] = accumulator[tossWinner] + 1 || 1;
            }
        }
        return accumulator;
    }, {});
    // it will return the result of team won toss and match both.
    return result;
}

module.exports = teamWonTossAndMatch;