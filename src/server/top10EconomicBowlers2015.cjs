// 4 - Top 10 economical bowlers in the year 2015

function top10EconomicBowlers2015(matches, deliveries) {
    // If any data JSON is empty or not an array it will return an empty object.
    if (matches === undefined || !Array.isArray(matches) || deliveries === undefined || !Array.isArray(deliveries)) {
        return {};
    }

    // This will store a set of unique match id of the year 2015.
    const uniqueId = matches.reduce((accumulator, match) => {
        const matchId = match.id;
        const season = Number(match.season);

        if (season === 2015) {
            accumulator.add(matchId);
        }
        return accumulator;
    },new Set());

    // It will create an object and store the bowler with their runs conceded, balls bowled and their economy.
    const result = deliveries.reduce((accumulator, delivery) => {
        const matchId = delivery.match_id;
        const bowler = delivery.bowler;
        const byeRuns = Number(delivery.bye_runs);
        const legByeRuns = Number(delivery.legbye_runs);
        const totalRuns = Number(delivery.total_runs);
        const totalBalls = Number(delivery.ball);

        // Check if the id matches the season 2015 and then it will store the player with their nested data.
        if (uniqueId.has(matchId)) {
            if (bowler in accumulator) {
                accumulator[bowler]['runs_conceded'] += (totalRuns - byeRuns - legByeRuns);
                if (totalBalls <= 6) {
                    accumulator[bowler]['total_balls'] += 1;
                }
            }
            else {
                accumulator[bowler] = {
                    runs_conceded: totalRuns - byeRuns - legByeRuns,
                    total_balls: 1,
                    economy: 0,
                };
            }
        }
        return accumulator;
    }, {});

    // It will convert the bowler Stat into an array, find and store the economy of each player, and then sort it in ascending order and return only the top 10 bowlers.
    const economyArray = Object.entries(result)
        .map((player) => {
            const totalRuns = player[1].runs_conceded;
            const totalBalls = player[1].total_balls;
            // Store the economy of each bowler and round it in 2 decimal places.
            player[1].economy = Number(((totalRuns/totalBalls) * 6).toFixed(2));
            return player;
        })
        .sort((playerA, playerB) => playerA[1].economy - playerB[1].economy)
        .slice(0, 10);

    // It will convert the economy array into object and return it.
    return Object.fromEntries(economyArray);
}

module.exports = top10EconomicBowlers2015;